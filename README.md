# 66-services

Credits:
- mobinmob <mobinmob@disroot.org>
- teldra <teldra@likeable.space>
- flexibeast <flexibeast@gmail.com>
- Eric Vidal <eric@obarun.org> 

License(s):

[ISC](ISC-LICENSE)

[BSD-2-Clause](LICENSE)
